package com.ms.utils;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.LinkedList;
import java.util.List;

import com.ms.activities.SequencerActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper {

	
	private static final int DATABASE_VERSION = 1;
	
	private static final String DATABASE_NAME = "TrackDB";

	public SQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_BOOK_TRACK = "CREATE TABLE tracks ( "
				+ "id INTEGER PRIMARY KEY AUTOINCREMENT, " + "source TEXT)";	
		db.execSQL(CREATE_BOOK_TRACK);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS tracks");	
		this.onCreate(db);
	}

	// ---------------------------------------------------------------------



	private static final String TABLE_TRACKS = "tracks";

	private static final String KEY_ID = "id";

	private static final String KEY_SOURCE = "source";

	private static final String[] COLUMNS = { KEY_ID, KEY_SOURCE };

	public void addTrack(Track track) {
		Log.d("addTrack", track.toString());
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_SOURCE, track.getSource());
		db.insert(TABLE_TRACKS,
				null, values);
		db.close();
	}

	public Track getTrack(int id) {

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_TRACKS, 
				COLUMNS, 
				" id = ?",
				new String[] { String.valueOf(id) }, 
				null, 
				null, 
				null, 
				null); 

		
		if (cursor != null)
			cursor.moveToFirst();

		
		Track track = new Track();
		track.setId(Integer.parseInt(cursor.getString(0)));
		track.setSource(cursor.getString(1));

		Log.d("getTrack(" + id + ")", track.toString());

		
		return track;
	}

	public List<Track> getAllTracks() {
		List<Track> tracks = new LinkedList<Track>();
		String query = "SELECT  * FROM " + TABLE_TRACKS;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		Track track = null;
		if (cursor.moveToFirst()) {
			do {
				track = new Track();
				track.setId(Integer.parseInt(cursor.getString(0)));
				track.setSource(cursor.getString(1));
				tracks.add(track);
			} while (cursor.moveToNext());
		}
		Log.d("getAllTracks()", track.toString());
		return tracks;
	}


	public int updateTrack(Track track, String path) {

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("source", path);

		
		int i = db.update(TABLE_TRACKS, 
				values, 
				KEY_ID + " = ?", 
				new String[] { String.valueOf(track.getId()) }); 

	
		db.close();

		return i;

	}

	
	public void deleteTrack(Track track) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_TRACKS, KEY_ID + " = ?",
				new String[] { String.valueOf(track.getId()) });
		db.close();
		Log.d("deleteTrack", track.toString());
}

	public void clearTable() {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.execSQL("delete from 'tracks'");
		// db.execSQL("delete from sqlite_sequence where name='tracks';");
	}

	public void createTable() {
		this.clearTable();
		for (int i = 1; i < 17; i++) {
			this.addTrack(new Track("null"));

		}
	}

	public void writeToSD(String DB_Path) throws IOException {

		String DB_PATH = DB_Path;
		File sd = Environment.getExternalStorageDirectory();

		if (sd.canWrite()) {
			String currentDBPath = DATABASE_NAME;
			String backupDBPath = "backupname.db";
			File currentDB = new File(DB_PATH, currentDBPath);
			File backupDB = new File(sd, backupDBPath);

			if (currentDB.exists()) {
				FileChannel src = new FileInputStream(currentDB).getChannel();
				FileChannel dst = new FileOutputStream(backupDB).getChannel();
				dst.transferFrom(src, 0, src.size());
				src.close();
				dst.close();
			}
		}
	}

}