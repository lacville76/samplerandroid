package com.ms.utils;
import java.util.*;
import java.io.*;

public class GetBytesFromFile {
        
        public static byte[] getBytesFromFile(File file) throws IOException {
                InputStream is = new FileInputStream(file);
               
                long length = file.length();
                
            
                if (length > Integer.MAX_VALUE) {
                        // File is too large
                }
                
             
                byte[] bytes = new byte[(int)length];
                
                // Read in the bytes
                int offset = 0;
                int numRead = 0;
                while (offset < bytes.length
                           && (numRead = is.read(bytes, offset, Math.min(bytes.length - offset, 512*1024))) >= 0) {
                        offset += numRead;
                }
                
                // Ensure all the bytes have been read in
                if (offset < bytes.length) {
                        throw new IOException("Could not completely read file "+file.getName());
                }
                
                // Close the input stream and return bytes
                is.close();
                return bytes;
        }
}