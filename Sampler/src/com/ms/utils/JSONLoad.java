package com.ms.utils;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;
public class JSONLoad {
  static InputStream is = null;
  static JSONObject jObj = null;
  static String json = "";
  String BPM;
	String beatTime;
	String totalBeats;
	String[] paths; 
	 boolean[][][] toggled ;
	 JSONArray[] listTracks = new JSONArray[18];

  public JSONLoad() {
  }
  public JSONObject getJSONFromUrl(String url) {

   
    	
    	  try {
			is = new FileInputStream(url);
		} catch (FileNotFoundException e1) {
		
			e1.printStackTrace();
		}
    try {
      BufferedReader reader = new BufferedReader(new InputStreamReader(
          is, "iso-8859-1"), 8);
      StringBuilder sb = new StringBuilder();
      String line = null;
      while ((line = reader.readLine()) != null) {
        sb.append(line + "n");
      }
      is.close();
      json = sb.toString();
    } catch (Exception e) {
      Log.e("Buffer Error", "Error converting result " + e.toString());
    }
   
    try {
      jObj = new JSONObject(json);
      JSONArray JSONpaths;
      BPM = jObj.getString("BPM");
      beatTime = jObj.getString("beatTime");
      totalBeats = jObj.getString("totalBeats");
      int length = Integer.valueOf(totalBeats);
      JSONObject json_data;
    toggled = new boolean[3][17][length];
      for(int i=0; i<3; i++){
    	 
    	  for(int z=1; z<17; z++){
    		  JSONpaths =jObj.getJSONArray("matrix"+String.valueOf(i)+String.valueOf(z));
    		    for(int y=0; y<length; y++){
    		    	toggled[i][z][y]=JSONpaths.getBoolean(y);
    		    }
      	    
      	} 
    	    
    	}
      
 
      
    } catch (JSONException e) {
      Log.e("JSON Parser", "Error parsing data " + e.toString());
    }
   
    return jObj;
  }
  public int getBeatTime()
  {
	  return Integer.valueOf(beatTime);
  }
  public int getTotalBeats()
  {
	  return Integer.valueOf(totalBeats);
  }
  public boolean[][][] getToggled()
  {
	  return toggled;
  }
  public int getBPM()
  {
	  return Integer.valueOf(BPM);
  }
  
  
  
}